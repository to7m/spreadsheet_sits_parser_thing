# spreadsheet_SITS_parser_thing

A python program accompanied by an input spreadsheet and config files. Generates another spreadsheet for use in the SITS system used by some universities. Automatically groups same events in different SITS weeks. See readme.txt for more.
