this should be intuitive enough, but I haven't tested it much

to use as normal:
    unzip the zip file if you haven't already
        but keep the original zip file to redistribute it
    read through a timetable and input the information to input.ods
        fill in the Academic Year first for each entry
    run main.exe
        if it gives a warning, click “more info”, then run anyway
    in the output file (output.ods):
        look through the first sheet (errors) carefully
        skim the following sheets maybe
        assuming the last sheet has the information you want:
            rename output.ods to something else
                (to make sure main.exe won't ever overwrite it)
            start inputting that data into the SITS online thing

to configure:
    all .conf files should open fine in Notepad
    sort_order.conf:
        reorder these lines to affect the last output sheet's order
    date_check_xxxx-xxxx.conf:
        specify which dates are valid/invalid
        to define a new academic year:
            copy date_check_xxxx-xxxx.conf
            rename the copy to have the correct new academic year
            edit the contents of the file

email any bugs, issues, questions, or
python script commissions to thomashowemain@gmail.com
