from collections import OrderedDict
from dateutil import parser, rrule
from datetime import date
from operator import itemgetter

from read_write_spreadsheet import dict_to_sheet


def format_academic_year(string, location, errors):
    for year in string[:4], string[5:]:
        try:
            int(year)
        except ValueError:
            errors.fatal(f'invalid academic year "{string}" in {location}')
    return f"{string[:4]}–{string[5:]}"


def return_sits_week(iso_date, sits_object):
    date_object = date.fromisoformat(iso_date)
    return (date_object - sits_object).days // 7 + 1


class Diary:
    def __init__(self, dir, errors):
        self.errors = errors
        self.file_contents = {}
        self.__read_dir__(dir)

    def __find_filenames__(self, dir):
        list_of_filenames = []
        for filename in [file.name for file in dir.iterdir()]:
            print(filename)
            if len(filename) == 25:
                if filename[:11] == "date_check_" and filename[20:] == ".conf":
                    list_of_filenames.append(filename)
        return list_of_filenames

    def __return_strings_in_categories__(self, file):
        strings_in_categories = {}
        lines = file.read_text(encoding="utf-8").split('\n')
        category = None
        for line in lines:
            while len(line) > 0 and '#' in line:
                del line[line.index('#'):]
            while len(line) > 0 and line[-1] == ' ':
                del line[-1]
            if len(line) == 0:
                continue
            if line[-1] == ':':
                category = line[:-1]
                strings_in_categories[category] = []
            else:
                if category is None:
                    self.errors.fatal(
                        f"first line in {file} is not a category")
                strings_in_categories[category].append(line)
        categories = ["Academic year", "UEA valid periods",
                      "UEA invalid periods", "National (bank) holidays",
                      "UEA customary holidays", "SITS week 1 Monday"]
        checklist = categories.copy()
        for category in strings_in_categories.keys():
            if category in categories:
                if category in checklist:
                    checklist.remove(category)
                else:
                    self.errors.fatal(f'category "{category}" declared'
                                      f" multiple times in {file}")
            else:
                self.errors.fatal(
                    f'unrecognised category "{category}" in {file}')
        return strings_in_categories

    def __format_dates_to_list__(self, string, file):
        units = string.split('/')
        if len(units) == 3:
            bounds = {"start": units, "end": units}
        elif len(units) == 5:
            dates = [date.split('/') for date in string.split(units[2][4])]
            bounds = {"start": dates[0], "end": dates[1]}
        else:
            self.errors.fatal(f'date entry "{string}" in {file} contains'
                              f"{units - 1} forward slashes")
        for position, units in bounds.items():
            date_dict = OrderedDict([("year", units[2]),
                                     ("month", units[1]), ("day", units[0])])
            date_string = ''
            for unit, number in date_dict.items():
                try:
                    int(number)
                except ValueError:
                    self.errors.fatal(f'invalid {unit} number "{number}"'
                                      f' in "{string}" in {file}')
                date_string += number.zfill(4 if unit == "year" else 2)
            bounds[position] = parser.parse(date_string)
        datetimes = rrule.rrule(rrule.DAILY,
                                dtstart=bounds["start"], until=bounds["end"])
        dates = [date(obj.year, obj.month, obj.day) for obj in list(datetimes)]
        return [obj.isoformat() for obj in dates]

    def __return_dates_in_categories__(self, strings_in_categories, file):
        for category in ["Academic year", "SITS week 1 Monday"]:
            if len(strings_in_categories[category]) != 1:
                self.errors.fatal(f"{file} must contain exactly 1"
                                  ' "Academic year" entry')
        dates_in_categories = {}
        for category, strings in strings_in_categories.items():
            dates = []
            for string in strings:
                dates += self.__format_dates_to_list__(string, file)
            dates.sort()
            dates_in_categories[category] = dates

        sits_date = dates_in_categories["SITS week 1 Monday"][0]
        sits_day = parser.parse(sits_date.replace('-', '')).strftime("%A")
        if sits_day != "Monday":
            self.errors.fatal(
                f"SITS week 1 Monday ({sits_date}) is a {sits_day}")

        return dates_in_categories, sits_date

    def __return_sorted_dict__(self, dictionary):
        items = dictionary.items()
        return_key_from_dict_item = itemgetter(0)
        sorted_items = sorted(items, key=return_key_from_dict_item)
        return dict(sorted_items)

    def __return_categories_in_dates__(self, dates_in_categories):
        categories_in_dates = {}
        for category, dates in dates_in_categories.items():
            for iso_date in dates:
                if iso_date not in categories_in_dates:
                    categories_in_dates[iso_date] = []
                categories_in_dates[iso_date].append(category)
        return self.__return_sorted_dict__(categories_in_dates)

    def __return_properties_of_dates__(
            self, categories_in_dates, academic_year):
        properties_of_dates = {}

        error = [''] * 8
        for index in [0b001, 0b010, 0b011]:
            error[index] = "not in academic year but "
        error[0b100] = "in academic year but "
        error[0b100] += "marked as neither invalid or valid period"
        error[0b001] += "marked as invalid period"
        error[0b010] += "marked as valid period"
        for index in [0b011, 0b111]:
            error[index] += "marked as both invalid and invalid period"
        period = (["not in academic year"] * 4
                  + [None, "UEA invalid period", "UEA valid period", None])

        for iso_date, categories in categories_in_dates.items():
            index = (("Academic year" in categories) << 2) \
                  + (("UEA valid periods" in categories) << 1) \
                  + (("UEA invalid periods" in categories))
            if error[index]:
                self.errors.fatal(f"date {iso_date} {error[index]}")
            parsed = parser.parse(iso_date.replace('-', ''))
            day = parsed.strftime("%d")
            day = day[(1 if day[0] == '0' else 0):]
            year_dict = self.file_contents[academic_year]
            sits_object = year_dict["SITS week 1 Monday date object"]
            sits_week = return_sits_week(iso_date, sits_object)
            properties_of_dates[iso_date] \
                = {"SITS week": sits_week, "weekday": parsed.strftime("%A"),
                   "period": period[index], "day": day,
                   "month": parsed.strftime("%B"),
                   "year": parsed.strftime("%Y")}
        return self.__return_sorted_dict__(properties_of_dates)

    def __handle_file__(self, dir, filename):
        file = dir / filename
        academic_year = format_academic_year(filename[11:20], f'"{file}"',
                                             self.errors)
        self.file_contents[academic_year] = ordered_dict = OrderedDict()

        strings_in_categories = self.__return_strings_in_categories__(file)
        ordered_dict["strings in categories"] = strings_in_categories

        dates_in_categories, sits_week_1_monday \
            = self.__return_dates_in_categories__(strings_in_categories, file)
        ordered_dict["dates in categories"] = dates_in_categories
        ordered_dict["SITS week 1 Monday date object"] \
            = date.fromisoformat(sits_week_1_monday)

        categories_in_dates = self.__return_categories_in_dates__(
                                  dates_in_categories)
        ordered_dict["categories in dates"] = categories_in_dates

        properties_of_dates = self.__return_properties_of_dates__(
                                  categories_in_dates, academic_year)
        ordered_dict["properties of dates"] = properties_of_dates

    def __read_dir__(self, dir):
        for filename in self.__find_filenames__(dir):
            self.__handle_file__(dir, filename)

    def return_ordered_dict(self):
        ordered_dict = OrderedDict()
        for academic_year, file_contents in self.file_contents.items():
            ordered_dict[f"{academic_year} — strings in categories"] \
                = dict_to_sheet(
                      file_contents["strings in categories"], False, False)
            ordered_dict[f"{academic_year} — dates in categories"] \
                = dict_to_sheet(
                      file_contents["dates in categories"], False, False)
            ordered_dict[f"{academic_year} — categories in dates"] \
                = dict_to_sheet(
                      file_contents["categories in dates"], True, False)
            properties = list(file_contents["properties of dates"].values())
            sheet = [list(properties[0].keys())]
            previous_period = None
            for dictionary in properties:
                if dictionary["period"] != previous_period:
                    sheet.append(['' for cell in range(len(sheet[0]))])
                    previous_period = dictionary["period"]
                sheet.append(list(dictionary.values()))
            ordered_dict[f"{academic_year} — properties of dates"] = sheet
        return ordered_dict
