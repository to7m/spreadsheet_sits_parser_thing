from pyexcel_ods import get_data, write_data
from collections import OrderedDict


def rectangle_of_strings(sheet, errors):
    if type(sheet) is not list:
        errors.fatal("invalid file type")
    width = 0
    height = 0
    row = 0
    while row < len(sheet):
        if type(sheet[row]) is not list:
            errors.fatal("invalid file type")
        column = 0
        while column < len(sheet[row]):
            if sheet[row][column] != '':
                width = max(width, column + 1)
                height = max(height, row + 1)
                sheet[row][column] = str(sheet[row][column])
            column += 1
        row += 1
    del sheet[height:]
    for row in sheet:
        if len(row) < width:
            row += [''] * (width - len(row))
        elif len(row) > width:
            del row[width:]
    return sheet


def read(file, errors):
    if not file.is_file():
        errors.fatal(f"file not found: {file}")
    try:
        ordered_dict = get_data(str(file))
    except OSError:
        errors.fatal(f"no such file: {file}")
    for sheet in ordered_dict:
        rectangle_of_strings(ordered_dict[sheet], errors)
    return ordered_dict


def write_ordered_dict(file, ordered_dict):
    write_data(str(file), ordered_dict)


def write(file, errors, *items):
    ordered_dict = OrderedDict()
    for item in items:
        if type(item) in [OrderedDict]:
            ordered_dict.update(item)
        else:
            try:
                ordered_dict.update(item.return_ordered_dict())
            except AttributeError:
                errors.fatal(f"unsupported item type: {type(item)}")
    write_ordered_dict(file, ordered_dict)


def dict_to_sheet(dictionary, as_rows=False, convert_to_str=True):
    if convert_to_str:
        items = dictionary.items()
        dictionary = {}
        for key, values in items:
            dictionary[str(key)] = [str(value) for value in values]
    keys = len(dictionary.keys())
    most = max([len(values) for values in dictionary.values()])
    if as_rows:
        sheet = [['' for column in range(most + 1)] for row in range(keys)]
        for key_number, key in enumerate(list(dictionary.keys())):
            sheet[key_number][0] = key
            for value_number, value in enumerate(dictionary[key]):
                sheet[key_number][value_number + 1] = value
    else:
        sheet = [['' for column in range(keys)] for row in range(most + 1)]
        sheet[0] = list(dictionary.keys())
        for key_number, values in enumerate(dictionary.values()):
            for value_number, value in enumerate(values):
                sheet[value_number + 1][key_number] = value
    return sheet
