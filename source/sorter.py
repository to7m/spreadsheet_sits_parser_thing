from collections import OrderedDict
from operator import itemgetter


class Sorter:
    def __init__(self, file, errors):
        self.errors = errors
        self.__read__(file)

    def __read__(self, file):
        if not file.exists():
            self.errors.fatal(f"path {file} does not exist")
        if not file.is_file():
            self.errors.fatal(f"path {file} is not a file")
        lines = file.read_text(encoding="utf-8").split('\n')

        specified_items = ["Academic year", "Module code", "Type of session",
                           "SITS weeks", "Day", "Start time", "Duration",
                           "Number of groups", "Room required — y/n"]
        checklist = specified_items.copy()
        self.sort_priority = []

        for line in lines:
            if line != '':
                if line not in specified_items:
                    self.errors.fatal(f'invalid item "{line}" in {file}')
                if line not in checklist:
                    self.errors.fatal(f'multiple "{line}" in {file}')
                self.sort_priority.append(line)
                checklist.remove(line)
        if checklist:
            self.errors.fatal(f"items {checklist} not found in {file}")

    def return_ordered_dict(self):
        ordered_dict = OrderedDict()
        ordered_dict["sort order"] = [[item] for item in self.sort_priority]
        return ordered_dict

    def __return_ranges_string__(self, list_of_numbers, row):
        ranges = [[list_of_numbers[0], list_of_numbers[0]]]
        for number in list_of_numbers[1:]:
            if number == ranges[-1][1]:
                self.errors.fatal(f"row {row} occurs multiple times")
            elif number == ranges[-1][1] + 1:
                ranges[-1][1] = number
            else:
                ranges.append([number, number])
        range_strings = []
        for range in ranges:
            first, last = str(range[0]), str(range[1])
            range_strings.append(first if first == last else f"{first}–{last}")
        return ','.join(range_strings)

    def __clump_sits__(self, contents, column_order, sits_column):
        previous_content = [contents[0][index] for index in column_order]
        sits_list = [contents[0][sits_column]]
        row = 1
        while row < len(contents):
            current_content = [contents[row][index] for index in column_order]
            if current_content == previous_content:
                sits_list.append(contents[row][sits_column])
                del contents[row]
            else:
                contents[row - 1][sits_column] \
                    = self.__return_ranges_string__(sits_list, contents[row])
                sits_list = [contents[row][sits_column]]
                previous_content = current_content
                row += 1
        contents[row - 1][sits_column] \
            = self.__return_ranges_string__(sits_list, contents[row - 1])

    def return_sorts_ordered_dict(self, concatenation_unsorted):
        contents = concatenation_unsorted[1:]
        for row, row_contents in enumerate(contents):
            contents[row] = row_contents.copy()
        days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"
                "Saturday", "Sunday"]
        ordered_dict = OrderedDict()

        column_order = list(range(len(self.sort_priority)))
        sits_column = self.sort_priority.index("SITS weeks")
        column_order.insert(0, column_order.pop(sits_column))
        days_column = self.sort_priority.index("Day")

        for row in contents:
            row[days_column] = days.index(row[days_column])
        for column in column_order:
            contents = sorted(contents, key=itemgetter(column))
        for row in contents:
            row[days_column] = days[row[days_column]]
        ordered_dict["concatenation — before SITS"] \
            = [self.sort_priority] + contents
        contents = contents.copy()
        for row, row_contents in enumerate(contents):
            contents[row] = row_contents.copy()

        self.__clump_sits__(contents, column_order[1:], sits_column)
        ordered_dict["concatenation — clumped SITS"] \
            = [self.sort_priority] + contents
        contents = contents.copy()
        for row, row_contents in enumerate(contents):
            contents[row] = row_contents.copy()

        for row in contents:
            row[days_column] = days.index(row[days_column])
        for column in range(len(self.sort_priority)):
            contents = sorted(contents, key=itemgetter(column))
        for row in contents:
            row[days_column] = days[row[days_column]]
        ordered_dict["concatenation — sorted"] \
            = [self.sort_priority] + contents

        return ordered_dict
