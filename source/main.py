from pathlib import Path

from read_write_spreadsheet import write
from errors import Errors
from sorter import Sorter
from diary import Diary
from read_and_format import read_and_format


def main():
    dir = Path.cwd()
    errors = Errors(dir / "output.ods")
    sorter = Sorter(dir / "sort_order.conf", errors)
    diary = Diary(dir, errors)
    formatted_input = read_and_format(
                          dir / "input.ods", sorter, diary, errors)
    sorted = sorter.return_sorts_ordered_dict(
                 formatted_input["concatenation — unsorted"])
    write(dir / "output.ods", errors,
          errors, sorter, diary, formatted_input, sorted)


main()
