from collections import OrderedDict
from dateutil import parser

from read_write_spreadsheet import read
from diary import format_academic_year, return_sits_week


def check_header(progress, name, sheet, errors):
    if len(sheet) == 0:
        errors.fatal(f'sheet "{name}" is empty')

    preserveds = ["Module code", "Academic year", "Type of session",
                  "Date", "Start time", "End time",
                  "Number of groups", "Room required — y/n"]
    discards = ["Day", "Duration"]
    checklist = preserveds.copy()
    progress["skimmed header"] = []
    progress["columns"] = {}
    for column, category in enumerate(sheet[0]):
        if category not in preserveds:
            if category in discards:
                continue
            errors.fatal(f'invalid category "{category}" in sheet "{name}"')
        if category not in checklist:
            errors.fatal(f'multiple "{category}" in sheet "{name}"')
        progress["skimmed header"].append(category)
        progress["columns"][category] = column
        checklist.remove(category)
    if checklist:
        errors.fatal(f'category "{checklist}" not found in sheet "{name}"')


def skim(progress, sheet):
    progress["skimmed"] = []
    row = 1
    while row < len(sheet):
        if all([item == '' for item in sheet[row]]):
            row += 1
            continue
        skimmed_row = []
        for category in progress["skimmed header"]:
            column = progress["columns"][category]
            skimmed_row.append(sheet[row][column])
        progress["skimmed"].append(skimmed_row)
        row += 1
    progress["columns"] = {}
    for column, category in enumerate(progress["skimmed header"]):
        progress["columns"][category] = column


def fill(progress, name, errors):
    if len(progress["skimmed"]) == 0:
        errors.fatal(f'sheet "{name}" has no entries')
    if any([cell == '' for cell in progress["skimmed"][0]]):
        errors.fatal(f'first entry in sheet "{name}" is incomplete')
    progress["filled"] = []
    for row, skimmed_row in enumerate(progress["skimmed"]):
        progress["filled"].append([])
        for column, skimmed_cell in enumerate(skimmed_row):
            if skimmed_cell == '':
                progress["filled"][-1].append(
                                           progress["filled"][row - 1][column])
            else:
                progress["filled"][-1].append(skimmed_cell)


def format_module_code(input_cell, input_row, errors):
    return input_cell


def format_type_of_session(input_cell, input_row, errors):
    return input_cell


def try_string_to_int(string, input_cell, input_row, intended_type, errors):
    try:
        int(string)
    except ValueError:
        errors.fatal(f'cell containing "{input_cell}" in {input_row}'
                     f" seems to not be formatted as {intended_type}")


def format_date(input_cell, input_row, errors):
    try_string_to_int(input_cell[:4] + input_cell[5:7] + input_cell[8:10],
                      input_cell, input_row, "a date", errors)
    return f"{input_cell[:4]}-{input_cell[5:7]}-{input_cell[8:10]}"


def format_time(input_cell, input_row, errors):
    try_string_to_int(input_cell[:2] + input_cell[3:5],
                      input_cell, input_row, "a time", errors)
    return f"{input_cell[:2]}:{input_cell[3:5]}"


def format_number_of_groups(input_cell, input_row, errors):
    try_string_to_int(input_cell, input_cell, input_row, "an integer", errors)
    return input_cell


def format_room_required(input_cell, input_row, errors):
    if input_cell not in ['y', 'n']:
        errors.fatal(f'cell containing "{input_cell}" in {input_row}'
                     " should be either 'y' or 'n'")
    return input_cell


def formatter(progress, errors):
    format_function_dict = {"Module code": format_module_code,
                            "Academic year": format_academic_year,
                            "Type of session": format_type_of_session,
                            "Date": format_date,
                            "Start time": format_time,
                            "End time": format_time,
                            "Number of groups": format_number_of_groups,
                            "Room required — y/n": format_room_required}
    progress["formatted dicts"] = []
    progress["formatted"] = []
    for row in progress["filled"]:
        progress["formatted dicts"].append({})
        progress["formatted"].append([])
        for column, filled_cell in enumerate(row):
            criterion = progress["skimmed header"][column]
            function = format_function_dict[criterion]
            formatted_cell = function(filled_cell, row, errors)
            progress["formatted dicts"][-1][criterion] = formatted_cell
            progress["formatted"][-1].append(formatted_cell)


def process_academic_year(formatted_dict, diary, errors):
    academic_year = formatted_dict["Academic year"]
    if academic_year not in diary.file_contents:
        errors.fatal(f"row {formatted_dict} specifies year {academic_year}"
                     f', but "date_check_{academic_year}.conf" not found')
    return academic_year


def process_module_code(formatted_dict, diary, errors):
    return formatted_dict["Module code"]


def process_type_of_session(formatted_dict, diary, errors):
    return formatted_dict["Type of session"]


def process_sits_week(formatted_dict, diary, errors):
    contents_of_year = diary.file_contents[formatted_dict["Academic year"]]
    date = formatted_dict["Date"]
    properties_of_date = contents_of_year["properties of dates"][date]
    period = properties_of_date["period"]
    if period != "UEA valid period":
        errors.regular(f"row {formatted_dict} flagged as {period}")
    sits_object = contents_of_year["SITS week 1 Monday date object"]
    return return_sits_week(date, sits_object)


def process_day(formatted_dict, diary, errors):
    day = parser.parse(formatted_dict["Date"].replace('-', '')).strftime("%A")
    if day in ["Saturday", "Sunday"]:
        errors.regular(f"row {formatted_dict} flagged as {day}")
    return day


def process_start_time(formatted_dict, diary, errors):
    return formatted_dict["Start time"]


def process_duration(formatted_dict, diary, errors):
    start, end = formatted_dict["Start time"], formatted_dict["End time"]
    start_minutes = int(start[:2]) * 60 + int(start[3:])
    end_minutes = int(end[:2]) * 60 + int(end[3:])
    duration_minutes = end_minutes - start_minutes
    duration_minutes %= 24 * 60
    hour = str(duration_minutes // 60).zfill(2)
    minute = str(duration_minutes % 60).zfill(2)
    return f"{hour}:{minute}"


def process_number_of_groups(formatted_dict, diary, errors):
    return formatted_dict["Number of groups"]


def process_room_required(formatted_dict, diary, errors):
    return formatted_dict["Room required — y/n"]


def process(progress, sorter, diary, errors):
    process_function_dict = {"Academic year": process_academic_year,
                             "Module code": process_module_code,
                             "Type of session": process_type_of_session,
                             "SITS weeks": process_sits_week,
                             "Day": process_day,
                             "Start time": process_start_time,
                             "Duration": process_duration,
                             "Number of groups": process_number_of_groups,
                             "Room required — y/n": process_room_required}
    progress["processed"] = []
    for formatted_dict in progress["formatted dicts"]:
        progress["processed"].append([])
        for criterion in sorter.sort_priority:
            function = process_function_dict[criterion]
            processed_cell = function(formatted_dict, diary, errors)
            progress["processed"][-1].append(processed_cell)


def read_and_format(file, sorter, diary, errors):
    input = read(file, errors)
    ordered_dict = OrderedDict()
    concatenation = [sorter.sort_priority]
    for name, sheet in input.items():
        progress = OrderedDict()
        check_header(progress, name, sheet, errors)
        skim(progress, sheet)
        fill(progress, name, errors)
        formatter(progress, errors)
        process(progress, sorter, diary, errors)
        concatenation += progress["processed"]
        progress["skimmed"].insert(0, progress["skimmed header"])
        progress["filled"].insert(0, progress["skimmed header"])
        progress["formatted"].insert(0, progress["skimmed header"])
        progress["processed"].insert(0, sorter.sort_priority)
        ordered_dict[f'input — "{name}" skimmed'] = progress["skimmed"]
        ordered_dict[f'input — "{name}" filled'] = progress["filled"]
        ordered_dict[f'input — "{name}" formatted'] = progress["formatted"]
        ordered_dict[f'input — "{name}" processed'] = progress["processed"]
    ordered_dict["concatenation — unsorted"] = concatenation
    return ordered_dict
