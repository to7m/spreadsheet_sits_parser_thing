from collections import OrderedDict

from read_write_spreadsheet import write_ordered_dict


class Errors:
    def __init__(self, fatal_error_path):
        self.fatal_error_path = fatal_error_path
        self.error_list = []

    def return_ordered_dict(self):
        ordered_dict = OrderedDict([("errors", [])])
        if not self.error_list:
            self.regular("no errors reported")
        for error in self.error_list:
            ordered_dict["errors"].append([error])
        return ordered_dict

    def fatal(self, error_message):
        if type(error_message) != str:
            error_message = f"{error_message} is not a string"
        self.error_list.append(error_message)
        entry = ("errors (see last entry for fatal error)",
                 [[error] for error in self.error_list])
        ordered_dict = OrderedDict([entry])
        write_ordered_dict(self.fatal_error_path, ordered_dict)
        raise SystemExit(f"FATAL ERROR: {error_message}")

    def regular(self, error_message):
        if type(error_message) != str:
            error_message = self.fatal(error_message)
        self.error_list.append(error_message)
        print(f"ERROR: {error_message}")
